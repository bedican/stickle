import StackTemplateParams from './types/StackTemplateParams';
const fs = require('fs');

export interface StackParamsData {
    Profile: string;
    Region: string;
    SecretsManagerId?: string;
    SecretsManagerRegion?: string;
    StackName?: string;
    Params?: StackTemplateParams
}

export default class StackParams {

    private path : string;
    private data : StackParamsData | null;

    constructor(path : string) {
        this.path = path;
        this.data = null;
    }

    public getData() : StackParamsData {
        if (this.data === null) {
            if (!fs.existsSync(this.path)) {
                throw new Error('Stack params file not found: ' + this.path);
            }

            this.data = JSON.parse(fs.readFileSync(this.path)) as StackParamsData;
        }

        return this.data;
    }

    public getRegion() : string {
        return this.getData().Region;
    }

    public getSecretsManagerRegion() : string {
        return this.getData().SecretsManagerRegion || this.getRegion();
    }

    public getProfile() {
        return this.getData().Profile;
    }

    public getStackName() : string {
        return this.getData().StackName || '';
    }

    public getSecretsManagerId() : string {
        return this.getData().SecretsManagerId || '';
    }

    public getStackTemplateParams() : StackTemplateParams {
        return this.getData().Params || {};
    }
}
