import * as AWS from 'aws-sdk';

import AwsConfig from './AwsConfig';
import StackParams from '../StackParams';
import StackTemplate from '../StackTemplate';
import StackTemplateParams from '../types/StackTemplateParams';

export abstract class EventHandler {
    public abstract handleEvent(event: AWS.CloudFormation.StackEvent) : void;
}

export default class AwsCloudFormationService {

    private service : AWS.CloudFormation;
    private lastEventId: string = '';

    public static async factory(params : StackParams) : Promise<AwsCloudFormationService> {
        return new AwsCloudFormationService(new AWS.CloudFormation(
            await AwsConfig.withCredentials(params.getProfile(), { region: params.getRegion() })
        ));
    }

    constructor(service : AWS.CloudFormation) {
        this.service = service;
    }

    public async exists(template : StackTemplate) : Promise<boolean> {

        const params : AWS.CloudFormation.GetTemplateInput = {
            StackName: template.getName()
        };

        try {
            await this.service.getTemplate(params).promise();
        } catch(e) {
            return false;
        }

        return true;
    }

    public async createOrUpdate(template : StackTemplate, templateParams : StackTemplateParams) : Promise<string> {

        const stackExists = await this.exists(template);

        if (!stackExists) {
            return this.create(template, templateParams);
        } else {
            return this.update(template, templateParams);
        }
    }

    public async create(template : StackTemplate, templateParams : StackTemplateParams) : Promise<string> {

        const params = [];
        for (const key in templateParams) {
            params.push({
                ParameterKey: key,
                ParameterValue: templateParams[key]
            });
        }

        const request : AWS.CloudFormation.CreateStackInput = {
            StackName: template.getName(),
            Capabilities: [ 'CAPABILITY_IAM' ],
            OnFailure: 'ROLLBACK',
            Parameters: params,
            TemplateBody: template.getContents(),
            TimeoutInMinutes: 1
        };

        const response : AWS.CloudFormation.CreateStackOutput = await this.service.createStack(request).promise();

        return response.StackId as string;
    }

    public async update(template : StackTemplate, templateParams : StackTemplateParams) : Promise<string> {

        const params = [];
        for (const key in templateParams) {
            params.push({
                ParameterKey: key,
                ParameterValue: templateParams[key]
            });
        }

        const request : AWS.CloudFormation.UpdateStackInput = {
            StackName: template.getName(),
            Capabilities: [ 'CAPABILITY_IAM' ],
            Parameters: params,
            TemplateBody: template.getContents()
        };

        const response : AWS.CloudFormation.UpdateStackOutput = await this.service.updateStack(request).promise();

        return response.StackId as string;
    }

    private async describeStackEvents(params: AWS.CloudFormation.DescribeStackEventsInput, handler: EventHandler) : Promise<any> {

        const response: AWS.CloudFormation.DescribeStackEventsOutput = await this.service.describeStackEvents(params).promise();

        let events: AWS.CloudFormation.StackEvent[] = [];
        let stackEvents = response.StackEvents || [];

        if (this.lastEventId) {
            for(const event of stackEvents) {
                if (event.EventId == this.lastEventId) {
                    break;
                }

                events.push(event);
            }
        } else {
            events = stackEvents;
        }

        if (events.length) {
            this.lastEventId = events[0].EventId;
        }

        events = events.reverse();
        for (const event of events) {
            handler.handleEvent(event);
        }

        const status: string[] = [
            'CREATE_COMPLETE',
            'CREATE_FAILED',
            'DELETE_COMPLETE',
            'DELETE_FAILED',
            'UPDATE_COMPLETE',
            'UPDATE_FAILED',
            'ROLLBACK_COMPLETE'
        ];

        if (events.length) {
            const firstEvent = events[0];
            if (firstEvent.ResourceType === 'AWS::CloudFormation::Stack') {
                if ((firstEvent.ResourceStatus) && status.indexOf(firstEvent.ResourceStatus) !== -1) {
                    return;
                }
            }
        }

        await this.sleep(1000);

        if (response.NextToken) {
            params.NextToken = response.NextToken;
        }

        this.describeStackEvents(params, handler);
    }

    private async sleep(ms: number) : Promise<void> {
        return new Promise<void>(resolve => {
            setTimeout(resolve, ms);
        });
    }

    public async getEvents(stackName: string, handler: EventHandler) {

        const params : AWS.CloudFormation.DescribeStackEventsInput = {
            StackName: stackName
        };

        this.describeStackEvents(params, handler);
    }
}
