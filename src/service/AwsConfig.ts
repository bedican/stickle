import * as AWS from 'aws-sdk';

export default class AwsConfig {
    public static async withCredentials(profile : string, options: object) : Promise<AWS.Config> {

        let config : AWS.Config = new AWS.Config({
            credentials: new AWS.SharedIniFileCredentials({ profile: profile })
        });

        if (options) {
            config.update(options);
        }

        return config;
    }
}
