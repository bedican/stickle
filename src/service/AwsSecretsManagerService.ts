import * as AWS from 'aws-sdk';

import AwsConfig from './AwsConfig';
import StackParams from '../StackParams';

export interface AwsSecretsManagerServiceValues {
    [name : string] : string;
}

export default class AwsSecretsManagerService {

    private service : AWS.SecretsManager;

    public static async factory(params : StackParams) : Promise<AwsSecretsManagerService> {
        return new AwsSecretsManagerService(new AWS.SecretsManager(
            await AwsConfig.withCredentials(params.getProfile(), { region: params.getSecretsManagerRegion() })
        ));
    }

    constructor(service : AWS.SecretsManager) {
        this.service = service;
    }

    public async getSecretValue(secretId : string) : Promise<AwsSecretsManagerServiceValues> {

        const params : AWS.SecretsManager.GetSecretValueRequest = {
            SecretId: secretId
        };

        const response : AWS.SecretsManager.GetSecretValueResponse = await this.service.getSecretValue(params).promise();

        if (!response.SecretString) {
            return {};
        }

        return JSON.parse(response.SecretString);
    }
}
