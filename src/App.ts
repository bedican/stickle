import Context from './Context';
import AwsCloudFormationService, { EventHandler } from './service/AwsCloudFormationService';
import AwsSecretsManagerService from './service/AwsSecretsManagerService';

class AppEventHandler extends EventHandler {
    public handleEvent(event: AWS.CloudFormation.StackEvent) {
        console.log(event.Timestamp.toISOString() + ' [' + event.LogicalResourceId + '] ' + event.ResourceStatus + ' ' + (event.ResourceStatusReason ? event.ResourceStatusReason : '...'));
    }
}

export default class App {
    public async run() {

        const context = Context.fromArgv(process.argv);

        const template = context.getStackTemplate();
        const stackParams = context.getStackParams();

        const templateParams = stackParams.getStackTemplateParams();
        const secretManagerId = stackParams.getSecretsManagerId();

        if (secretManagerId) {
            const secretsManager = await AwsSecretsManagerService.factory(stackParams);
            const secrets = await secretsManager.getSecretValue(secretManagerId);

            for (const key in secrets) {
                templateParams[key] = secrets[key];
            }
        }

        const cloudFormation = await AwsCloudFormationService.factory(stackParams);
        const stackId = await cloudFormation.createOrUpdate(template, templateParams);

        const eventHandler = new AppEventHandler();

        await cloudFormation.getEvents(stackId, eventHandler);
    }
}
