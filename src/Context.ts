import StackTemplate from './StackTemplate';
import StackParams from './StackParams';

export default class Context {

    private stack : string;
    private environment: string;

    private template : StackTemplate | null;
    private params : StackParams | null;

    constructor(stack : string, environment : string) {
        this.stack = stack;
        this.environment = environment;
        this.template = null;
        this.params = null;
    }

    public getStack() : string {
        return this.stack;
    }

    public getEnvironment() : string {
        return this.environment;
    }

    public getStackTemplate() : StackTemplate {
        if (this.template == null) {
            const params = this.getStackParams();
            const stackName = params.getStackName() || this.stack;

            this.template = new StackTemplate(stackName, process.cwd() + '/stacks/' + this.stack);
        }

        return this.template;
    }

    public getStackParams() : StackParams {
        if (this.params == null) {
            this.params = new StackParams(process.cwd() + '/params/' + this.environment + '/' + this.stack + '.json');
        }

        return this.params;
    }

    public static fromArgv(argv : string[]) {

        argv.shift();
        argv.shift();

        const stack = argv.shift();
        if (!stack) {
            throw new Error('Missing stack');
        }

        const environment = argv.shift();
        if (!environment) {
            throw new Error('Missing environment');
        }

        return new Context(stack, environment);
    }
}
