const fs = require('fs');

const supportedFormats : string[] = ['yml', 'yaml', 'json'];

export default class StackTemplate {

    private name : string;
    private path : string;
    private contents : string = '';
    private loaded : boolean = false;

    constructor(name: string, path : string) {
        this.name = name;
        this.path = path;
    }

    public getName() : string {
        return this.name;
    }

    public getContents() {
        if (this.loaded) {
            return this.contents;
        }

        for(const key in supportedFormats) {
            const format = supportedFormats[key];
            const filename = this.path + '.' + format;

            if (fs.existsSync(filename)) {
                this.contents = fs.readFileSync(filename).toString();
                this.loaded = true;

                return this.contents;
            }
        }

        throw new Error('Template not found: ' + this.path + '.[' + supportedFormats.join('|') + ']');
    }
}
