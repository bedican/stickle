export default interface StackTemplateParams {
    [key : string]: string
}
