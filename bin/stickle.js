#! /usr/bin/env node

const App = require('../dist/App').default;

const app = new App();

(async () => {
    await app.run();
})().catch(e => {
    console.error(e.message);
    process.exit(1);
});
