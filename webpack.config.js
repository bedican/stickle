const path = require('path');
const glob = require('glob');

const tsFiles = glob.sync('./src/**/*.ts');
const entries = {};

for(let x in tsFiles) {
    entries[tsFiles[x].replace(/(^\.\/src\/)|(\.ts$)/g, '')] = tsFiles[x];
}

module.exports = {
    mode: 'production',
    entry: entries,
    resolve: {
        extensions: ['.js', '.json', '.ts'],
    },
    optimization: {
        minimize: true
    },
    output: {
        libraryTarget: 'commonjs',
        path: path.join(__dirname, 'dist'),
        filename: '[name].js',
    },
    target: 'node',
    module: {
        rules: [
            { test: /\.ts$/, loader: 'ts-loader' },
        ],
    },
    externals: {
    }
};
