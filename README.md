# Stickle

Stickle is a command line tool for creating aws CloudFormation stacks.

# Installation

```bash
$ npm install -g stickle
```

# Usage

```bash
$ stickle <stack> <environment>
```

# Application Layout

Stickle apps are structured as shown below, an example app can be seen within the `example-apps` directory of the repository.

Stickle should be run from within the app top level directory.

```
MyApp
- params
- - <environment>
- - - <template parameters file>.json
- stacks
- - <stack template file>.(yml|json)
```

## Stacks

CloudFormation templates (either yml, json or a mixture) should be defined within the `stacks` directory.

As they should be identical between environments, there is no separation within the directory structure. In the case whereby you wish to use computed values beyond the parameter files, you should take advantage of mappings and conditions.

## Stack Parameters

Stack parameters will differ between environment, and are defined within the `./params/<environment>` directory, with the file name matching the same name as the stack template within the `stacks` directory but with a `.json` extension.

An example parameters file is shown below, the `profile` and `region` is defined at parameter level (and thus environment level) so that environments can exist across regions.

```json
{
    "Profile": "my-credentials-profile",
    "Region": "eu-west-1",
    "SecretsManagerId": "my-secret-manager-production",
    "SecretsManagerRegion": "eu-west-1",
    "Params": {
        "BucketName": "production-my-company-my-bucket"
    }
}
```

### Non-secret parameters

Non-secret template parameters are define within the `Params` section of the parameters file, with a name/value pair with the name matching the parameter name within the stack template file.

### Secret parameters

Secret parameters, which should not be committed to a source code repository, are maintained within a secret manager with the ID defined with the `SecretsManagerId` option.

The secret manager should be created within the same region as the stack, with the `Region` parameter. However, not all regions have the secret manager available, if this is the case with your region, you may specify a `SecretsManagerRegion`.

The secret manager should contain name/value pairs with the name matching the parameter name within the stack template file.

If a duplicate name exists within the secret manager to that of the parameters file, the secret managers value will take precedence.

## Credentials

Stickle uses a profile within the [aws cli credentials file](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html) which is defined within the stack parameters file.

#### License: MIT
#### Author: [Ash Brown](https://bitbucket.org/bedican)
